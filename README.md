# README #

This is a program that performs K-Means clustering on Cytometry data including Outlier detection.
It tries to identify cells that exhibit similar behaviour, which may identify if a cell group is cancerous.
